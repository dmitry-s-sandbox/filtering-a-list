import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, map, Observable, of, shareReplay, switchMap } from 'rxjs';

interface Car {
  vin: string;
  brand: string;
  year: number;
  color: string;
}

interface CarData {
  data: Car[];
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public cars$: Observable<Car[]> = of([]);
  public filteredCars$: Observable<Car[]> | undefined;
  private search$$: BehaviorSubject<string> = new BehaviorSubject('');
  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit(): void {
    this.cars$ = this.http.get<CarData>('assets/data/cars-large.json').pipe(map(cars => cars.data)).pipe(
      shareReplay(1),
    );

    let searchStr: string | null;

    this.filteredCars$ = this.search$$.pipe(
      switchMap(search => {
        searchStr = search;
        return this.cars$;
      }),
      map(cars => searchStr ? cars.filter(car => car.brand.toLowerCase().includes((searchStr as string).toLowerCase())) : cars),
    )
  }

  filter(search: string): void {
    this.search$$.next(search);
  }
}
